/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

// Import libraries
import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View
} from 'react-native';
import {
    Footer,
    FooterTab,
    Container,
    Button,
    Icon
} from 'native-base';
import HomeComponent from './src/components/HomeComponent';
import ProfileComponent from './src/components/ProfileComponent';
import i18n from './src/components/I18n';
import timeoutPromise from './src/utils/utils';


// Create `Wemissu` react component
var Wemissu = React.createClass({

    getInitialState: function() {
        return {
            reports: [],
            countryName: '',
            regionName: '',
            countryCode: '',
            currentViewIndex: 0,
            refreshing: false,
            page: 0,
            pageLimit: 5,
            filteredSearch: false,
            searchText: '',
            noResults: false,
            facebookUserData: {
                fbUserID: null,
                fbFirstName: null,
                fbLastName: null,
                fbPicture: null,
                fbCover: null
            }
        }
    },

    componentDidMount: function() {
        this.getLocationData();
    },

    componentWillMount: function() {

        this._onSearch = (text) => {
            this.setState({
                refreshing : true,
                searchText: text
            }, () => {
                this._onSearchCallback();
            })
        };

        this._onSearchCallback = _.debounce( () => {
            const text = this.state.searchText;

            if(text.length === 0) {
                this.setState({
                    page: 0,
                    refreshing: true,
                    reports: [],
                    filteredSearch: false
                }, () => {
                    this.getAllReports();
                })
            }else{
                this.setState({
                    page: 0,
                    refreshing: true,
                    reports: [],
                    filteredSearch: true
                }, () => {
                    this.getReportsBySearchString();
                })
            }
        }, 1000);
    },

    render: function() {
        let ViewComponent = null;

        if (this.state.currentViewIndex === 0) {
            ViewComponent = <HomeComponent reports={ this.state.reports }
                                           countryName={ this.state.countryName }
                                           countryCode={ this.state.countryCode }
                                           refreshing={ this.state.refreshing }
                                           onRefresh={ this._onRefresh }
                                           onSearch={ this._onSearch }
                                           onEndReached={ this._handleEndReached }
                                           searchText={ this.state.searchText }></HomeComponent>
        } else {
            ViewComponent = <ProfileComponent
                                onFacebookLogin={ this._handleFacebookLogin }
                                onFacebookLogout={ this._handleFacebookLogout }
                                facebookUser={ this.state.facebookUserData }>
                            </ProfileComponent>
        }

        return (
            <Container style={{ backgroundColor: '#E9EBEE'}}>
                { ViewComponent }
                <Footer>
                    <FooterTab>
                        <Button onPress={() => this.switchScreen(0) }>
                            <Icon style={{
                                fontSize: 32,
                                fontWeight: '100',
                                color: '#333333'
                            }} name={ this._setTabActiveStateIcon('home') } />
                        </Button>
                        <Button onPress={() => this.switchScreen(1) }>
                            <Icon style={{
                                fontSize: 32,
                                fontWeight: '100',
                                color: '#333333'
                            }} name={ this._setTabActiveStateIcon('person') } />
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    },

   /*
    * This function is used to change the icon's style depending on his active state.
    */

    _setTabActiveStateIcon: function(iconName) {
        var icon = 'ios-' + iconName;
        if (this.state.currentViewIndex === 0 && iconName === 'person') {
            icon += '-outline';
        }else if (this.state.currentViewIndex === 1 && iconName === 'home') {
            icon += '-outline';
        }

        return icon;
    },

   /*
    * Switch screen from 'Home' to 'My Profile' and viceversa
    * @params index {Int}
    */
    switchScreen: function(index) {
        this.setState(
            { currentViewIndex: index }
        );
    },

   /*
    * This will fetch() all the reports by Country name paginated.
    */

    getAllReports: function() {
        const { page , pageLimit, countryName } = this.state;
        const url = Constants.API_URL + `/reports?country=${countryName}&page=${page}&pageLimit=${pageLimit}`;

        timeoutPromise(5000, fetch(url))
        .then((response) => response.json())
        .then((responseJson) => {

            if(responseJson.length === 0) {
                this.setState({ noResults: true });
            }else{
                this.setState({ noResults: false });
            }

            this.setState({
                reports: page === 0 ? responseJson : [ ...this.state.reports, ...responseJson ],
                refreshing: false
            });

        })
        .catch((error) => {
            console.log(error);
            alert(i18n.t('unableToRetrieveDataMessage'));
            this.setState({ refreshing: false });
        });
    },

   /*
    * This will fetch() all the reports that are typed in the Search Input paginated.
    */

    getReportsBySearchString: function() {
        const { page , pageLimit, countryName, searchText } = this.state;
        const url = Constants.API_URL + `/reports?country=${countryName}&name=${searchText}&page=${page}&pageLimit=${pageLimit}`;

        timeoutPromise(5000, fetch(url))
        .then((response) => response.json())
        .then((responseJson) => {

            if(responseJson.length === 0) {
                this.setState({ noResults: true });
            }else{
                this.setState({ noResults: false });
            }

            this.setState({
                reports: page === 0 ? responseJson : [ ...this.state.reports, ...responseJson ],
                refreshing: false
            });

        })
        .catch((error) => {
            console.log(error);
            alert(i18n.t('unableToRetrieveDataMessage'));
            this.setState({ refreshing: false });
        });
    },

   /*
    * Retrieves the user's location data.
    */

    getLocationData: function() {
        if (this.state.countryName.length > 0) {
            this.getAllReports();
            return;
        }

        timeoutPromise(5000, fetch(Constants.GEO_URL))
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                countryName: responseJson.country_name,
                regionName: responseJson.region_name,
                countryCode: responseJson.country_code
            });
        })
        .then(() => {
            this.getAllReports();
        })
        .catch((error) => {
            console.log('Failed to get location data');
        });
    },

   /*
    * This function is called by children components by the `pull up to refresh` feature.
    */

    _onRefresh: function() {
        this.setState({
            page: 0,
            refreshing: true,
            reports: [],
            filteredSearch: false,
            searchText: '',
            noResults: false
        }, () => {
            this.getAllReports();
        })
    },

   /*
    * This Event handler is called when reaching the end of the report list.
    */

    _handleEndReached: function() {
        if(!this.state.noResults) {
            this.setState({
                page: this.state.page + 1
            }, () => {
                if (this.state.filteredSearch) {
                    this.getReportsBySearchString();
                }else{
                    this.getAllReports();
                }
            })
        }
    },

    _handleFacebookLogin: function(fbuser) {
        let fbObject =
        {
            fbUserID: fbuser.id,
            fbFirstName: fbuser.first_name,
            fbLastName: fbuser.last_name,
            fbPicture: fbuser.picture.data.url,
            fbCover: fbuser.cover.source
        }


        this.setState({
            facebookUserData: fbObject
        });
    },

    _handleFacebookLogout: function(data) {
        this.setState({
            facebookUserData: {
                fbUserID: null,
                fbFirstName: null,
                fbLastName: null,
                fbPicture: null,
                fbCover: null
            }
        })
    }

});

// Register `Wemissu` component
AppRegistry.registerComponent('wemissu', () => Wemissu);