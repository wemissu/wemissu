/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

import React, { Component } from 'react';
import Modal from 'react-native-modal';
import { Text,
         TouchableOpacity,
         View,
         Button,
         StyleSheet,
         Image,
         ActivityIndicator } from 'react-native';
import styles from '../styles/modal.style';
import { InputGroup, Icon, Input } from 'native-base';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import i18n from './I18n';
import imagePick from '../helpers/imagePicker';
import uploadFile from '../helpers/uploadFile';
import timeoutPromise from '../utils/utils';

var AddEntryModal = React.createClass({
    getInitialState: function() {
        return {
            fullName: '',
            age: '',
            location: '',
            description: '',
            phoneNumber: '',
            date: '',
            imageThumbnail: null,
            imageUploadData: null,
            imageUrl: null,
            imageName: null,
            loadingSubmit: false
        }
    },

    render: function() {
      return (
          <View style={ styles.modalContainer }>
              <Modal isVisible={ this.props.modalVisible } style={ styles.modal } >
                <View style={{ flex: 1 }}>
                  { this._renderModalContent() }
                </View>
              </Modal>
          </View>
      )
    },

    _renderButton: function(text, onPress, customStyles, isCancel) {
        return (
            <TouchableOpacity onPress={ onPress } style={ customStyles }>
              <View style={ !isCancel ? styles.modalButton : styles.modalButtonCancel }>
                <Text style={ styles.modalButtonText }>{ text }</Text>
              </View>
            </TouchableOpacity>
        )
    },

    _renderActivityIndicator: function() {
        if ( this.state.loadingSubmit ) {
            return (
                <ActivityIndicator
                    animating={ this.state.loadingSubmit }
                    style={[ styles.activityLoading , { height: 80 } ]}
                    size="large"
                />
            )
        }else{
            return null;
        }
    },

    _renderModalForm: function() {
        if ( !this.state.loadingSubmit ) {
            let imgThumbnail = this.state.imageThumbnail == null?
                <View style={ styles.imageThumbnail }>
                </View>
            :
                <Image
                source={ this.state.imageThumbnail }
                style={ styles.imageThumbnail }
                />

            return (
                <View style={ styles.modalContent }>
                    <InputGroup borderType="regular">
                        <Input placeholder={ i18n.t('fullName') } style={{
                            fontSize: 14,
                            padding: 0,
                            height: 30,
                            marginTop: 15,
                            fontWeight: '100'
                        }}
                        placeholderTextColor="#3B5998"
                        autoCorrect={ false }
                        value={ this.state.fullName }
                        onChangeText={(text) => this.setState({ fullName: text}) }/>
                    </InputGroup>

                    <InputGroup borderType="regular" >
                        <Input placeholder={ i18n.t('age') } style={{
                            fontSize: 14,
                            padding: 0,
                            height: 30,
                            marginTop: 15,
                            fontWeight: '100'
                        }}
                        placeholderTextColor="#3B5998"
                        maxLength= { 2 }
                        keyboardType={ 'numeric' }
                        returnKeyType={ 'next' }
                        value={ this.state.age }
                        onChangeText={(text) => this.setState({ age: text}) }/>
                    </InputGroup>

                    <InputGroup borderType="regular" >
                        <Input editable={ false } placeholder={ this.props.countryName } style={{ fontSize: 14, padding: 0, height: 30, marginTop: 15}}
                                placeholderTextColor="#C3C3C3"/>
                    </InputGroup>

                    <InputGroup borderType="regular" style={{ marginTop: 10, borderColor: 'transparent', marginBottom: 10 }}>
                        { imgThumbnail }
                        { this._renderButton(i18n.t('pickImage'), () => this._pickImage(), { alignSelf: 'flex-end'} )}
                    </InputGroup>

                    <DatePicker
                        style={{ width: 200 }}
                        date={ this.state.date }
                        mode="date"
                        placeholder={ i18n.t('lastTimeSeen') }
                        placeholderTextColor="#3B5998"
                        format="YYYY-MM-DD"
                        minDate={ this._getMinDate() }
                        maxDate={ this._getMaxDate() }
                        confirmBtnText={ i18n.t('confirm') }
                        cancelBtnText={ i18n.t('cancel') }
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                          },
                          dateInput: {
                            marginLeft: 36,
                            borderBottomWidth: StyleSheet.hairlineWidth,
                            borderTopWidth: 0,
                            borderLeftWidth: 0,
                            borderRightWidth: 0
                          }
                        }}
                        onDateChange={ (date) => { this._onDateChanged(date) } }
                    />

                    <InputGroup borderType="regular" >
                        <Input placeholder={ i18n.t('lastLocation') } style={{
                            fontSize: 14,
                            padding: 0,
                            height: 30,
                            marginTop: 15,
                            fontWeight: '100'
                        }} 
                        placeholderTextColor="#3B5998"
                        autoCorrect={ false }
                        value={ this.state.location }
                        onChangeText={(text) => this.setState({ location: text}) }/>
                    </InputGroup>

                    <InputGroup borderType="regular" >
                        <Input placeholder={ i18n.t('description') } style={{
                            fontSize: 14,
                            padding: 0,
                            height: 30,
                            marginTop: 15,
                            fontWeight: '100'
                        }}
                        placeholderTextColor="#3B5998"
                        maxLength= { 150 }
                        autoCorrect={ false }
                        value={ this.state.description }
                        onChangeText={(text) => this.setState({ description: text}) }/>
                    </InputGroup>

                    <InputGroup borderType="regular" >
                        <Input placeholder={ i18n.t('contactPhoneNumber') } style={{
                            fontSize: 14,
                            padding: 0,
                            height: 30,
                            marginTop: 15,
                            fontWeight: '100'
                        }}
                        placeholderTextColor="#3B5998"
                        autoCorrect={ false }
                        keyboardType={ 'phone-pad' }
                        returnKeyType={ 'next' }
                        value={ this.state.phoneNumber }
                        onChangeText={(text) => this.setState({ phoneNumber: text}) }/>
                    </InputGroup>

                    <InputGroup style={{ borderColor: 'transparent', marginTop: 20 }}>
                        { this._renderButton(i18n.t('close'), () => { this._onModalClose(true, false, null, false) }, { alignSelf: 'center', marginRight: 20}, true)}
                        { this._renderButton(i18n.t('confirm'), () => { this._submitData() }, { alignSelf: 'center', marginLeft: 20} )}
                    </InputGroup>
                </View>
            )
        }else {
            return null;
        }
    },

    _renderModalContent: function() {
        return (
            <View style={ styles.modalContent }>
                <Text style={ styles.modalTitle } >{ i18n.t('registerMissingPerson') }</Text>

                { this._renderActivityIndicator() }

                { this._renderModalForm() }
            </View>
        )
    },

    _onModalClose: function(resetData, showStatusModal, message) {

        if(resetData) {
            this.setState({
                fullName: '',
                age: '',
                location: '',
                description: '',
                phoneNumber: '',
                date: '',
                imageThumbnail: null,
                imageUploadData: null,
            });
        }

        this.props.closeModal(showStatusModal, message);
    },

    _pickImage: function() {
        imagePick((source, data) => this.setState({ imageThumbnail: source, imageUploadData: data}));
    },


    _submitData: function() {
        this.setState({ loadingSubmit: true });

        timeoutPromise(5000, this._saveAllFormData())
        .catch((error) => {
            this.setState({ loadingSubmit: false }, () => {
                this.props.handlePostStatus(true);
                this._onModalClose(false, true, i18n.t('failToSaveErrorMessage'));
            });
        })
    },

    _saveAllFormData: function() {
        var fieldErrors = this._validateMissingFields();

        if(fieldErrors.length > 0) {
            this._showMissingFieldsError(fieldErrors);
            return;
        }

        return uploadFile([
          { name: 'uploaded_picture',
            type: 'image/png',
            filename: 'uploaded_picture.png',
            data: this.state.imageUploadData
          }
        ])
        .then(res => {
            var data = JSON.parse(res.data);
            this.setState(
                {
                  imageUrl: data.image_url,
                  imageName: data.image_name,
                }
            );

            return this._sendFormDataToDB();
        })
        .then(() => {
            this.setState({ loadingSubmit: false }, () => {
                this.props.handlePostStatus(false);
                this._onModalClose(true, true, i18n.t('successToSaveMessage'));
            });

        })
        .catch(err => {
            this.setState({ loadingSubmit: false }, () => {
                this.props.handlePostStatus(true);
                this._onModalClose(false, true, i18n.t('failToSaveErrorMessage'));
            });
        })
    },

    _sendFormDataToDB: function(data) {
        var url = Constants.API_URL + '/reports/save';
        return fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                status: 'missing',
                image_url: this.state.imageUrl,
                image_name: this.state.imageName,
                date_posted: new Date(),
                date_lts: moment(this.state.date).format(),
                name: this.state.fullName,
                age: this.state.age,
                location: this.state.location,
                description: this.state.description,
                contact_phone: this.state.phoneNumber,
                country: this.props.countryName,
                approved: false
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
        })
        .catch((error) => {
            alert('There was a problem trying to save the report. Check your connection and try again.');
            //XXX: Do rollback and delete image from server
        })
    },

    _showMissingFieldsError: function(fieldErrors) {
        var errorMessage = ''
        for(var i = 0; i < fieldErrors.length; i++) {
            errorMessage = errorMessage + ', ' + fieldErrors[i];

        }
        errorMessage += '.';
        errorMessage = errorMessage.substring(2, errorMessage.length);
        errorMessage = i18n.t('missingFieldsErrorMessage') + ' ' + errorMessage;

        alert(errorMessage);
    },

    _validateMissingFields: function() {
        var aux = [];

        if (this.state.fullName.length === 0) {
            aux.push(i18n.t('fullName'));
        }

        if (this.state.description.length === 0) {
            aux.push(i18n.t('description'));
        }

        if (this.state.phoneNumber.length === 0) {
            aux.push(i18n.t('contactPhoneNumber'));
        }

        if (this.state.date.length === 0) {
            aux.push(i18n.t('lastTimeSeen'));
        }

        if (!this.state.imageUploadData) {
            aux.push(i18n.t('image'));
        }

        return aux;
    },

    _onDateChanged: function(date) {
        this.setState({date: date});
    },

    _getMinDate: function() {
        return moment().subtract(5, 'years').format('YYYY-MM-DD');
    },

    _getMaxDate: function() {
        return moment().format('YYYY-MM-DD');
    }

});

module.exports = AddEntryModal;