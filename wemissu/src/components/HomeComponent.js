/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

import React, { Component } from 'react';
import {
    ScrollView,
    View,
    RefreshControl,
    Text,
    ActivityIndicator,
    FlatList
} from 'react-native';
import Modal from 'react-native-modal';
import AddEntryModal from './AddEntryModal';
import StatusModal from './StatusModal';
import ReportItem from './ReportItem';
import i18n from './I18n';
import styles from '../styles/home.style';
import Flag from 'react-native-flags';
import {
    Header,
    Container,
    Left,
    Button,
    Icon,
    Body,
    Title,
    Right,
    Item,
    Input,
    InputGroup
} from 'native-base';
Constants = require('../utils/constants');

var HomeComponent = React.createClass({

    getInitialState: function() {
        return {
            modalVisible: false,
            statusModalVisible: false,
            statusModalMessage: '',
            reportPostFailed: false
        }
    },

    render: function() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#FF4D4D' }}>
                    <Left>
                    </Left>
                    <Body>
                        <InputGroup rounded style={{
                                height: 25,
                                width: 180,
                                paddingTop: 2,
                                borderColor: '#FFFFFF'
                            }}>
                                <Icon style={{
                                    marginTop: 2,
                                    marginLeft: -5,
                                    fontSize: 20,
                                    color: '#FFFFFF'
                                }} active name='ios-search' />
                                <Input style={{
                                    height: 20,
                                    marginLeft: -5,
                                    fontSize: 14,
                                    color: '#FFFFFF',
                                    borderColor: '#FFFFFF',
                                    fontWeight: '100'
                                }}
                                placeholder={ i18n.t('searchSomeone') }
                                value={ this.props.searchText }
                                placeholderTextColor={'rgba(255,255,255,0.5)'}
                                onChangeText={ this.props.onSearch }
                                defaultValue={''}/>
                        </InputGroup>
                    </Body>
                    <Right>
                        <Button onPress={ () => { this._showModal() }} transparent>
                            <Icon style={{
                                fontSize: 32,
                                fontWeight: '100',
                                color: '#FFFFFF'
                            }} name='ios-person-add' />
                        </Button>
                    </Right>
                </Header>
                <View style={ styles.titleWrapper }>
                    <Text style={ styles.title }>
                       { i18n.t('missingPeopleReportsFrom') }
                    </Text>
                    <Text style={ styles.subtitle }>
                        <Flag style={ styles.flag } type={'flat'} code={ this.props.countryCode } size={24} /> { this.props.countryName }
                    </Text>
                </View>
                <View style={ styles.modalContainers }>
                    <AddEntryModal
                        closeModal={ this._closeModal }
                        modalVisible={ this.state.modalVisible }
                        countryName={ this.props.countryName }
                        handlePostStatus={ this._handlePostStatus }
                        ></AddEntryModal>
                    <StatusModal
                        statusModalVisible={ this.state.statusModalVisible }
                        closeModal={ this._closeStatusModal }
                        message={ this.state.statusModalMessage }
                        ></StatusModal>
                </View>
                <FlatList
                    data={ this.props.reports }
                    renderItem={ this._renderItemComponent }
                    keyExtractor={  item => item._id }
                    refreshing={ this.props.refreshing }
                    onRefresh={ this.props.onRefresh }
                    onEndReached={ this.props.onEndReached }
                    onEndThreshold={ 0.8 }
                    >
                </FlatList>
            </Container>
        )
    },

    _renderItemComponent: ({item}) => {
        return (
            <View>
                <ReportItem item={ item } />
            </View>
        )
    },

    _showModal: function() {
        return this.setState({
            modalVisible: true,
            reportPostFailed: false
        });
    },

    _closeModal: function(openStatusModal, message) {
        this.setState({ modalVisible: false });

        if (openStatusModal) {
            setTimeout(() => {
                this._showStatusModal(message);
            }, 500);
        }
    },

    _showStatusModal: function(message) {
        this.setState({
            statusModalVisible: true,
            statusModalMessage: message
        });
    },

    _closeStatusModal: function() {
        this.setState({ statusModalVisible: false }, () => {
            if (this.state.reportPostFailed ) {
                setTimeout(() => {
                    this._showModal();
                }, 500);
            }
        });
    },

    listReports: function() {
        if (this.props.reports.length > 0) {
            return this.props.reports.map(function(report, key) {
                // Called N amount of times depending on array.length
                return (
                    <View key={ key }>
                        <LazyloadView host="reportsList">
                            <ReportItem item={ report } />
                        </LazyloadView>
                    </View>
                )
            });
        }else{
            if(this.props.refreshing) {
                return this._renderActivityIndicator();
            }else{
                return this._renderErrorTextBlock();
            }
        }
    },

    _renderActivityIndicator: function() {
        return (
            <ActivityIndicator
                animating={ this.props.refreshing }
                style={[ styles.activityLoading , { height: 80 } ]}
                size="small"
            ></ActivityIndicator>
        )
    },

    _renderErrorTextBlock: function() {
        return <Text style={ styles.statusMessage }>{ i18n.t('noMissingPeople') }</Text>
    },

    _handlePostStatus: function(failed) {
        this.setState({ reportPostFailed: failed });
    }

});

module.exports = HomeComponent;