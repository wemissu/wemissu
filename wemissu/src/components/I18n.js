const { NativeModules } = require('react-native');
const { RNI18n } = NativeModules;
import I18n from 'react-native-i18n';

I18n.fallbacks = true;
I18n.translations = {
    en: {
        searchSomeone: 'Search for someone',
        missingPeopleReportsFrom: 'Missing people reports from',
        dateReported: 'Reported on',
        name: 'Name',
        location: 'Location',
        lastTimeSeen: 'Last Time Seen',
        description: 'Description',
        contactPhone: 'Contact Phone',
        registerMissingPerson: 'Register a missing person',
        fullName: 'Full Name',
        age: 'Age',
        lastLocation: 'Last Location',
        contactPhoneNumber: 'Contact Phone Number',
        close: 'Close',
        confirm: 'Confirm',
        cancel: 'Cancel',
        pickImage: 'Pick Image',
        noMissingPeople: 'No missing people found in your country',
        loading: 'Loading...',
        profile: 'My Profile',
        selectPhoto: 'Select photo of missing person',
        image: 'Image',
        missingFieldsErrorMessage: 'These fields are missing:',
        failToSaveErrorMessage: 'Your report has failed to be saved. Please, check your connection and try again.',
        successToSaveMessage: 'Your report has been successfully sent.',
        unableToRetrieveDataMessage: 'There was a problem trying to retrieve data. Please, check your connection and try again.',
        loginWithFacebook: 'Log-in with Facebook',
        logoutWithFacebook: 'Log-out from Facebook',
        welcome: 'Welcome',
        loginDescription: 'In order to upload missing people reports, you need to log-in with Facebook.'
    },
    es: {
        searchSomeone: 'Buscar una persona',
        missingPeopleReportsFrom: 'Reportes de gente desaparecida en',
        dateReported: 'Reportado el día',
        name: 'Nombre',
        location: 'Ubicación',
        lastTimeSeen: 'Última vez visto/a',
        description: 'Descripción',
        contactPhone: 'Teléfono de Contacto',
        registerMissingPerson: 'Registrar un desaparecido',
        fullName: 'Nombre Completo',
        age: 'Edad',
        lastLocation: 'Última Ubicación',
        contactPhoneNumber: 'Teléfono de Contacto',
        close: 'Cerrar',
        confirm: 'Confirmar',
        cancel: 'Cancelar',
        pickImage: 'Elegir imágen',
        noMissingPeople: 'No se han encontrado datos de gente desaparecida en tu país',
        loading: 'Cargando...',
        profile: 'Mi Perfil',
        selectPhoto: 'Selecciona la foto de la persona desaparecida',
        image: 'Imágen',
        missingFieldsErrorMessage: 'Falta completar los siguientes campos:',
        failToSaveErrorMessage: 'Hubo un problema intentando guardar tu reporte. Por favor, verifique su conexión e intente nuevamente.',
        successToSaveMessage: 'Tu report ha sido enviado satisfactoriamente.',
        unableToRetrieveDataMessage: 'Hubo un problema intentando obtener los reportes. Por favor, verifique su conexión e intente nuevamente.',
        loginWithFacebook: 'Accedé con Facebook',
        logoutWithFacebook: 'Salir de Facebook',
        welcome: 'Bienvenido',
        loginDescription: 'Para poder registrar personas desaparecidas, necesitas conectarte a la aplicación mediante Facebook.'
    },
    pt: {
        searchSomeone: 'Pesquiçar uma pessoa',
        missingPeopleReportsFrom: 'Reportes de gente desaparecida em',
    },
    ru: {
        searchSomeone: 'Mother russia',
        missingPeopleReportsFrom: 'Huehueheuehu'
    }
}

module.exports = I18n;