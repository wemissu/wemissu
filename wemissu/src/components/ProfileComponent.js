/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

import React, { Component } from 'react';
import {
    Text,
    View,
    FlatList,
    TouchableOpacity,
    Image
} from 'react-native';
import i18n from './I18n';
import styles from '../styles/profile.style';
import {
    Header,
    Container,
    Icon,
    Body,
    Item
} from 'native-base';

import FBSDK, {
    LoginManager,
    AccessToken,
    GraphRequestManager,
    GraphRequest
} from 'react-native-fbsdk';

// import FBSDKCore, {
//     AccessToken
// } from 'react-native-fbsdkcore';

var ProfileComponent = React.createClass({

    componentDidMount: function() {

    },

    componentWillMount: function() {
        this._getFacebookUserData();
    },

   /*
    * Function to handle Facebook Authentication and permission to public-profile.
    * Check for more permission access through: https://developers.facebook.com/docs/facebook-login/permissions
    */

    _fbAuthenticate: function() {
        LoginManager.logInWithReadPermissions(['public_profile']).then((result) => {
            if(result.isCancelled) {
                console.log('Login cancelled');
            }else {
                console.log('Login success: ' + result.grantedPermissions);

                this._getFacebookUserData();
            }
        }, (error) => {
            console.log('An error occurred: ' + error);
        })
    },

    _fbLogout: function() {
        LoginManager.logOut();
        this.props.onFacebookLogout();
    },

    _getFacebookUserData: function() {
        AccessToken.getCurrentAccessToken()
        .then((data) => {
            let accessToken = data.accessToken.toString();
            const responseInfoCallback = (error, result) => {
                if (error) {
                    console.log('Error while fetching data from facebook user', error);
                } else {
                    console.log('Fetched results ', result);
                    this.props.onFacebookLogin(result);
                }
            }

            const infoRequest = new GraphRequest('/me', {
                accessToken: accessToken,
                parameters: {
                    fields: {
                        string: 'id,first_name,last_name,cover,picture.type(large)'
                    }
                }
            }, responseInfoCallback);

            // Start the graph request.
            new GraphRequestManager()
                .addRequest(infoRequest)
                .start()

        })
        .catch((error) => {
            // Means user is not logged in
            console.log('Error trying to fetch facebook user data');
        });
    },

    render: function() {
        return (
            <Container>
                <Header style={{ backgroundColor: '#FF4D4D' }}>
                    <Body>
                        <Text style={ styles.headerText }>{ i18n.t('profile') }</Text>
                    </Body>
                </Header>

                { this._renderProfilePicture() }

                { this._renderWelcomeAndLogin() }

                { this._renderUserReports() }
            </Container>
        )
    },

    _renderUserReports: function() {
        return <FlatList></FlatList>;
    },

    _renderProfilePicture: function() {
        if(!this.props.facebookUser.fbUserID) {
            return (
                <View style={ styles.profileContainerDefault }>
                    <Image resizeMode='cover' style={ styles.profileImageDefault }
                            source={ require('../assets/images/profile_user_template.png') } >
                    </Image>
                    <View style={ styles.profileTextWrapper }>
                        <Text style={ styles.firstNameText }>John </Text>
                        <Text style={ styles.lastNameText }>Doe</Text>
                    </View>
                </View>
            )
        }else{
            return (
                <Image resizeMode='cover' style={ [styles.coverPicture, styles.profileContainer] }
                    source={{ uri: this.props.facebookUser.fbCover }}>

                    <Image resizeMode='cover' style={ styles.profileImage }
                            source={{ uri: this.props.facebookUser.fbPicture }} >
                    </Image>
                    <View style={ styles.profileTextWrapper }>
                        <Text style={ styles.firstNameText }>{ this.props.facebookUser.fbFirstName } </Text>
                        <Text style={ styles.lastNameText }>{ this.props.facebookUser.fbLastName }</Text>
                    </View>
                </Image>
            )
        }
    },

    _renderWelcomeAndLogin: function() {
        return (
            <View>
                <View style={ styles.wrapper }>
                    <Text style={ styles.welcomeText }>{ i18n.t('welcome') }</Text>
                    <Text style={ styles.loginDescriptionText }>{ i18n.t('loginDescription') }</Text>

                    <TouchableOpacity onPress={ () => { this._fbAuthenticate() } }>
                        <View style={ styles.facebookButton }>
                            <View style={ styles.iconWrapper }>
                                <Icon style={{ fontSize: 18, fontWeight: '100', color: '#FFFFFF', height: 20, width: 20 }} name='logo-facebook' />
                            </View>
                            <Text style={ styles.facebookLoginText }>{ i18n.t('loginWithFacebook') }</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={ () => { this._fbLogout() } }>
                        <Text style={ styles.facebookLogoutText }>{ i18n.t('logoutWithFacebook') }</Text>
                    </TouchableOpacity>

                </View>


            </View>
        )
    }
});

module.exports = ProfileComponent;