/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

// Import libraries
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View
} from 'react-native';
import styles from '../styles/reports.style';
import i18n from './I18n';
import moment from 'moment';

// Create component
var ReportItem = React.createClass({
    render: function() {
        return (
            <View style={ styles.container } >
                <Text style={ styles.textSmall } >
                    { i18n.t('dateReported') }: { moment(this.props.item.date_posted).format('YYYY-MM-DD') }
                </Text>
                <Image resizeMode='cover' style={ styles.image }
                        source={{ uri: this.props.item.image_url }} >
                </Image>
                <Text style={ styles.textNormal } >
                    <Text style={ styles.textBold } >{ i18n.t('name') }: </Text> { this.props.item.name }, { this.props.item.age }
                </Text>
                <Text style={ styles.textNormal } >
                    <Text style={ styles.textBold } >{ i18n.t('location') }: </Text> { this.props.item.location }
                </Text>
                <Text style={ styles.textNormal } >
                    <Text style={ styles.textBold } >{ i18n.t('lastTimeSeen') }: </Text> { moment(this.props.item.date_lts).format('YYYY-MM-DD') }
                </Text>
                <Text style={ styles.textNormal } >
                    <Text style={ styles.textBold } >{ i18n.t('description') }: </Text> { this.props.item.description }
                </Text>
                <Text style={ styles.textNormal } >
                    <Text style={ styles.textBold } >{ i18n.t('contactPhone') }: </Text> { this.props.item.contact_phone }
                </Text>
            </View>
        );
    }
});

module.exports = ReportItem;