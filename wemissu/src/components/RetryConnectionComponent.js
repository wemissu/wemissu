/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

import React, { Component } from 'react';
import {
    View,
    Text,
    Button
} from 'react-native';
import i18n from './I18n';
import styles from '../styles/retry.connection.style';

var RetryConnectionComponent = React.createClass({
    // getInitialState: function() {
    //     return {

    //     }
    // },

    componentDidMount: function() {

    },

    render: function() {
        return (
            <Button onPress={ this._refreshReportList() } >
                <Icon name='ios-refresh'></Icon>
                <Text>Connection error. Tap to refresh</Text>
            </Button>
        )
    },

    _refreshReportList: function() {
        alert('clicked! Refreshing!');
    }
});

module.exports = RetryConnectionComponent;