/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

import React, { Component } from 'react';
import Modal from 'react-native-modal';
import { Text,
         TouchableOpacity,
         View,
         Button,
         StyleSheet,
         Image } from 'react-native';
import styles from '../styles/modal.style';
import { InputGroup, Icon, Input } from 'native-base';
import i18n from './I18n';

var StatusModal = React.createClass({

    render: function() {
      return (
          <View>
              <Modal isVisible={ this.props.statusModalVisible }
                     style={ styles.statusModalContainer  }
                     animationIn={'slideInUp'}
                     animationOut={'slideOutDown'}
                     >
                <View style={{ flex: 1 }}>
                  { this._renderModalContent() }
                </View>
              </Modal>
          </View>
      )
    },

    _renderButton: function(text, onPress, customStyles) {
        return (
            <TouchableOpacity onPress={ onPress } style={ customStyles }>
              <View style={ styles.modalButton }>
                <Text style={ styles.modalButtonText }>{ text }</Text>
              </View>
            </TouchableOpacity>
        )
    },

    _renderModalContent: function() {
         return (
            <View style={ styles.modalContent }>
                <Text style={ styles.statusText }> { this.props.message } </Text>
                { this._renderButton(i18n.t('close'), () => { this._onModalClose() }, { alignSelf: 'center' }) }
            </View>
         );
    },

    _onModalClose: function() {
        this.props.closeModal();
    }
});

module.exports = StatusModal;