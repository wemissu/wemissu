import ImagePicker from 'react-native-image-picker';
import i18n from '../components/I18n';

var options = {
        title: i18n.t('selectPhoto'),
        // customButtons: [
        //     {name: 'fb', title: 'Choose Photo from Facebook'},
        // ],
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
}


let pickImage = (cb) => {
    ImagePicker.showImagePicker(options, (response) => {
        if (response.didCancel) {
            console.log('User cancelled image picker');
        }
        else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        }
        // else if (response.customButton) {
        //   console.log('User tapped custom button: ', response.customButton);
        // }
        else {
            let source = { uri: response.uri };
            cb(source, response.data);
        }
    });
}

module.exports = pickImage;