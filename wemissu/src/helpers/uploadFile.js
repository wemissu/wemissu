import RNFetchBlob from 'react-native-fetch-blob';
Constants = require('../utils/constants');

let uploadFile = (data) => {
  return RNFetchBlob.fetch('POST', Constants.UPLOAD_URL, {
    Authorization : "Bearer access-token",
    'Content-Type' : 'multipart/form-data',
  }, data);
}

module.exports = uploadFile;