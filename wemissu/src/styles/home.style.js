import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    statusMessage: {
        color: '#333333',
        fontSize: 18,
        textAlign: 'center',
        marginTop: 50,
        fontWeight: '100',
    },
    title: {
        marginTop: 10,
        textAlign: 'center',
        color: '#333333',
        fontWeight: '100'
    },
    subtitle: {
        marginTop: 7,
        textAlign: 'center',
        color: '#333333',
        fontWeight: '400'
    },
    flag: {
        marginTop: 3
    },
    statusMessage : {
        color: '#333333',
        fontSize: 18,
        textAlign: 'center',
        marginTop: 50,
        fontWeight: '100',
    },
    titleWrapper: {
        backgroundColor: '#F3F3F3',
        borderBottomColor: '#D3D3D3',
        borderBottomWidth: StyleSheet.hairlineWidth,
        height: 65
    },
    modalContainers: {
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row'
    },
    activityLoading: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    }
});