/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  modalContainer: {
    position: 'absolute',
    marginTop: 50
  },
  statusModalContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  modalButton: {
    backgroundColor: '#3B5998',
    padding: 12,
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  modalButtonCancel: {
    backgroundColor: '#D9534F',
    padding: 12,
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  modalButtonText: {
    color: '#FFFFFF',
    fontWeight: '100'
  },
  modalContent: {
    backgroundColor: '#FFFFFF',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)'
  },
  modal: {
    marginTop: 20
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0
  },
  modalTitle: {
    fontSize: 18
  },
  imageThumbnail: {
    height: 50,
    width: 50,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#D3D3D3',
    borderRadius: 5
  },
  statusText: {
    textAlign: 'center',
    fontSize: 12
  },
  activityLoading: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8
  }
});