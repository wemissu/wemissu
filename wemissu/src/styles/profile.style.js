/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    headerText: {
        fontSize: 18,
        color: '#FFFFFF',
        fontWeight: '400'
    },
    profileContainerDefault: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3',
        borderBottomColor: '#D3D3D3',
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    profileContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    coverPicture: {
        height: null,
        width: null
    },
    profileImageDefault: {
        marginTop: 20,
        height: 100,
        width: 100,
        borderRadius: 50,
        borderColor: '#3B5998',
        opacity: 0.8,
        borderWidth: StyleSheet.hairlineWidth
    },
    profileImage: {
        marginTop: 20,
        height: 100,
        width: 100,
        borderRadius: 50,
        borderColor: '#444444',
        borderWidth: 2
    },
    profileTextWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#444444',
        marginTop: 10,
        marginBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        height: 20,
        borderRadius: 4
    },
    firstNameText: {
        fontSize: 18,
        fontWeight: '100',
        color: '#FFFFFF'
    },
    lastNameText: {
        fontSize: 18,
        fontWeight: '400',
        color: '#FFFFFF'
    },
    loginContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'column',
        margin: 20
    },
    welcomeText: {
        marginTop: 30,
        marginBottom: 10,
        fontSize: 20,
        color: '#444444',
        fontWeight: '200',
        textAlign: 'center'
    },
    loginDescriptionText: {
        fontSize: 12,
        color: '#444444',
        fontWeight: '100',
        textAlign: 'center',
        marginBottom: 30
    },
    container: {
        flex: 1
    },
    wrapper: {
        paddingHorizontal: 35
    },
    facebookButton: {
        backgroundColor: '#3B5998',
        paddingVertical: 15,
        marginVertical: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4
    },
    facebookLoginText: {
        fontSize: 12,
        color: '#FFFFFF',
        fontWeight: '100',
        marginLeft: 5
    },
    iconWrapper: {
        paddingHorizontal: 3,
        alignItems: 'center',
        justifyContent: 'center'
    }
});