/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center', // coord y
        alignItems: 'center', // coord x
        margin: 15,
        overflow: 'hidden',
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: '#d3d3d3',
        padding: 5,
        borderRadius: 3,
        backgroundColor: '#f3f3f3'
    },
    image: {
        width: 400,
        height: 300
    },
    textNormal: {
        fontSize: 12,
        color: '#444444',
        alignSelf: 'flex-start',
        marginTop: 10,
        fontWeight: '100'
    },
    textBold: {
        fontSize: 14,
        color: '#444444',
        alignSelf: 'flex-start',
        marginTop: 10,
        fontWeight: '400'
    },
    textSmall: {
        fontSize: 12,
        color: '#A0A0A0',
        fontStyle: 'italic',
        alignSelf: 'flex-end',
        fontWeight: '100',
        marginBottom: 3
    }
});
