/*
 * WeMissU App - All Rights Reserved @ 2017
 * @author Mariano Marzullo
*/



let local_env = 'http://localhost:3000';
let external_env = 'http://192.168.1.12:3000';
let api_path = '/api';
let upload_path = '/upload';

module.exports = {
    // REPORTS : [
    //     {
    //         status: 'missing',
    //         image: 'https://payload446.cargocollective.com/1/0/19261/11232668/rottenburger_800.jpg',
    //         date_posted: '04/05/2017',
    //         date_lts: '01/05/2017', //lts = last time seen
    //         name: 'John Doe',
    //         age: 36,
    //         location: 'Brooklyn, NY',
    //         description: 'He was wearing blue t-shirt.',
    //         contact_phone: '1-800-333-5555',
    //         country: 'United States of America'
    //     },
    //     {
    //         status: 'missing',
    //         image: 'https://payload446.cargocollective.com/1/0/19261/11232668/rottenburger_800.jpg',
    //         date_posted: '04/05/2017',
    //         date_lts: '01/05/2017', //lts = last time seen
    //         name: 'Sarah Doe',
    //         age: 18,
    //         location: 'Central Park, NY',
    //         description: 'He was wearing yellow t-shirt.',
    //         contact_phone: '1-800-333-5555',
    //         country: 'United States of America'
    //     },
    //     {
    //         status: 'missing',
    //         image: 'https://payload446.cargocollective.com/1/0/19261/11232668/rottenburger_800.jpg',
    //         date_posted: '04/05/2017',
    //         date_lts: '01/05/2017', //lts = last time seen
    //         name: 'Nikola Doe',
    //         age: 25,
    //         location: 'Buenos Aires, Argentina',
    //         description: 'He was wearing red sweater.',
    //         contact_phone: '1133994400',
    //         country: 'Argentina'
    //     }
    // ],

    GEO_URL: 'https://freegeoip.net/json/',

    API_URL: external_env + api_path,

    UPLOAD_URL: external_env + upload_path

    // API_URL: local_env + api_path,

    // UPLOAD_URL: local_env + upload_path
}