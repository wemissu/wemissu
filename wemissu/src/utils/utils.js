/**
  * Timeout Promise
  * @param ms {integer} Miliseconds for timeout
  * @param promise {Promise} Promise to execute
  * Example of usage:
  *  timeoutPromise(1000, fetch('/hello')).then(function(response) {
  *    // process response
  *  }).catch(function(error) {
  *    // might be a timeout error
  *  })
**/

let timeoutPromise = function(ms, promise) {
    return new Promise((resolve, reject) => {
        const timeoutId = setTimeout(() => {
        reject(new Error("promise timeout"))
        }, ms);
        promise.then(
            (res) => {
                clearTimeout(timeoutId);
                resolve(res);
            },
            (err) => {
                clearTimeout(timeoutId);
                reject(err);
            }
        );
    });
}

module.exports = timeoutPromise;


